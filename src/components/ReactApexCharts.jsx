import * as React from 'react';

import loadable from '@loadable/component'

const ReactApexCharts = loadable(() => import('react-apexcharts'));


export default ReactApexCharts;